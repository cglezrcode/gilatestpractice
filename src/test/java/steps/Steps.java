package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.PageManager;


public class Steps {
    PageManager pageManager;
    String BetaProURL = "https://beta-loanpro.simnang.com/client/app/login.html?";
    public Steps(PageManager pageManager){
        this.pageManager = pageManager;
    }

    @Given("User goes into beta LoanPro login page")
    public void userGoesIntoBetaLoanProLoginPage(){
        pageManager.gotoURL(BetaProURL);
    }

    @When("User introduces credentials and perform login")
    public void userIntroducesCredentialsAndPerformLogin(){
        pageManager.getLoginPage().loginWithTestCredentials();

    }

    @Then("Loan Manager page is displayed")
    public void loanManagerPageIsDisplayed() {
        Assert.assertTrue("LoanPro Home page is not ready"
                ,pageManager.getHomePage().isPageLoaded());
    }

    @When("User selects approved status on Loan status filter")
    public void userSelectsApprovedStatusOnLoanStatusFilter() {
        pageManager.getHomePage().selectLoanStatusFilter("Approved");
    }

    @Then("Approved loans are displayed into the Loan Manager page")
    public void approvedLoansAreDisplayedIntoTheLoanManagerPage() {
        Assert.assertTrue("Rows with wrong status or the Table is empty"
                ,pageManager.getHomePage().validateStatusRows("Approved"));
    }

}
