package steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import pages.PageManager;

public class Hooks {

    PageManager pageManager;
    public Hooks(PageManager pageManager){
        this.pageManager = pageManager;
    }
    @Before
    public void before(){
        System.out.println("RUNNING AUTOMATION");
    }
    @After
    public void pageLogout() {
        pageManager.getHomePage().logOut();
        pageManager.closeDriver();
    }
}