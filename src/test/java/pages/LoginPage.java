package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends GenericPage{

    private String testUser="loanproqaautomatedapitesting+atestpractice@gmail.com";
    private String testPassword="%Wd5u50Q1?1215";

    @FindBy(id="username")
    private WebElement userName;

    @FindBy (id="password")
    private WebElement password;

    @FindBy(xpath = "//button[contains(text(),'Login')]")
    private WebElement loginButton;

    public LoginPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
        wait = new WebDriverWait(driver,60);
    }

    public boolean loginWithTestCredentials(){
        wait.until(ExpectedConditions.elementToBeClickable(userName));
        wait.until(ExpectedConditions.visibilityOf(loginButton));
        userName.clear();
        password.clear();
        userName.sendKeys(testUser);
        password.sendKeys(testPassword);
        loginButton.click();
        return true;
    }

}
