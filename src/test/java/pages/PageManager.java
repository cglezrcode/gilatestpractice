package pages;

import driver.DriverUtil;
import org.openqa.selenium.WebDriver;

public class PageManager {
    private WebDriver driver;
    private LoginPage loginPage;
    private HomePage homePage;

    public PageManager(){
        this.driver= DriverUtil.setupChromeWebDriver();
    }

    public LoginPage getLoginPage(){
        return loginPage == null? loginPage = new LoginPage(driver):loginPage;
    }

    public HomePage getHomePage(){
        return homePage == null? homePage = new HomePage(driver):homePage;
    }

    public void closeDriver (){
        driver.close();
    }

    public WebDriver getDriver(){
        return driver;
    }

    public void gotoURL(String URL){
        driver.get(URL);
    }


}
