package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GenericPage {
    public static WebDriver driver;
    public static WebDriverWait wait;
    public GenericPage(WebDriver driver){
        this.driver = driver;
    }

    public boolean isWebElementPresent(By locator){
        return !driver.findElements(locator).isEmpty();
    }

    public boolean isWebElementPresent(WebElement element, By locator){
        return !element.findElements(locator).isEmpty();
    }

    public boolean isWebElementNull(WebElement element){
        return element.equals(null);
    }

    public boolean isDisplayedandEnabled(WebElement element){
        return element.isDisplayed() && element.isEnabled();
    }

}
