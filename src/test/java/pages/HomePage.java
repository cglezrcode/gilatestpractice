package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends GenericPage{

    @FindBy(xpath="//span[contains(text(),'Loans')]")
    private WebElement loansSpan;

    @FindBy(xpath="//img[@class='circle-image' and @alt]")
    private WebElement circleImage;

    @FindBy(xpath = "//button[@title='New Loan']")
    private WebElement newLoanButton;

    @FindBy(xpath="//md-icon[contains(@class,'new-lp-icon-excel-reports')]")
    private WebElement excelReportsIcon;

    @FindBy(xpath = "//span[contains(@class,'tenant-title')]")
    private WebElement testingTenantSpan;

    @FindBy(id="loanStatus")
    private WebElement loanStatusSelect;

    @FindBy(id="loanManagerTable")
    private WebElement resultsTable;

    @FindBy(xpath="//table[@id='loanManagerTable']/tbody/tr")
    private WebElement firstResultRow;

    @FindBy(xpath="//md-whiteframe[@title='Total number of accounts']")
    private WebElement totalAccountsFrame;

    @FindBy(xpath = "(//button[@title='Next page'])[1]")
    private WebElement nextPageButton;


    public HomePage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
        wait = new WebDriverWait(driver,60);
    }

    public boolean isPageLoaded(){
        wait.until (ExpectedConditions.visibilityOf(loanStatusSelect));
        wait.until(ExpectedConditions.visibilityOf(resultsTable));
        if (!isDisplayedandEnabled(loansSpan)) return false;
        if (!isDisplayedandEnabled(circleImage))return false;
        if (!isDisplayedandEnabled(newLoanButton))return false;
        if (!isDisplayedandEnabled(excelReportsIcon))return false;
        if (!isDisplayedandEnabled(testingTenantSpan))return false;
        return true;
    }

    public void selectLoanStatusFilter(String status){
        loanStatusSelect.click();
        WebElement option = driver.findElement(By.xpath("//md-option/div[contains(text(),'"+status+"')]"));
        JavascriptExecutor je = (JavascriptExecutor) driver;
        wait.until(ExpectedConditions.elementToBeClickable(option));
        je.executeScript("arguments[0].scrollIntoView(true);",option);
        option.click();
    }

    public boolean validateStatusRows(String status){
        wait.until(ExpectedConditions.visibilityOf(resultsTable));
        wait.until(ExpectedConditions.visibilityOf(firstResultRow));
        //this is the worst idea to work with waits, it should not be an option but for now it was the only way I found to have a constant run
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        String accounts = driver.findElement(By.xpath("//md-whiteframe[@title='Total number of accounts']"))
                                .getText()
                                .split("\n")[1];
        int rows = driver.findElements(By.xpath("//td[@class='ng-binding' and contains(text(),'Approved')]")).size();
        return accounts.equals(Integer.toString(rows));
    }

    public void logOut(){
        circleImage.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//md-menu-content[contains(@class,'user-profile-menu')]")));
        driver.findElement(By.xpath("//a[@ng-click=\"signOut()\"]")).click();
    }


}
