Feature: Search Approved Loans

  @SmallPractice
  Scenario: Login into Loan manager and looking for Approved Loans
      Given User goes into beta LoanPro login page
      When User introduces credentials and perform login
      Then Loan Manager page is displayed
      When User selects approved status on Loan status filter
      Then Approved loans are displayed into the Loan Manager page



